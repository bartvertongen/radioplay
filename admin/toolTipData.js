var FiltersEnabled = 0; // if your not going to use transitions or filters in any of the tips set this to 0
var spacer="&nbsp; &nbsp; &nbsp; ";

// email notifications to admin
notifyAdminNewMembers0Tip=["", spacer+"No email notifications to admin."];
notifyAdminNewMembers1Tip=["", spacer+"Notify admin only when a new member is waiting for approval."];
notifyAdminNewMembers2Tip=["", spacer+"Notify admin for all new sign-ups."];

// visitorSignup
visitorSignup0Tip=["", spacer+"If this option is selected, visitors will not be able to join this group unless the admin manually moves them to this group from the admin area."];
visitorSignup1Tip=["", spacer+"If this option is selected, visitors can join this group but will not be able to sign in unless the admin approves them from the admin area."];
visitorSignup2Tip=["", spacer+"If this option is selected, visitors can join this group and will be able to sign in instantly with no need for admin approval."];

// radiostation table
radiostation_addTip=["",spacer+"This option allows all members of the group to add records to the 'Radio Station' table. A member who adds a record to the table becomes the 'owner' of that record."];

radiostation_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Radio Station' table."];
radiostation_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Radio Station' table."];
radiostation_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Radio Station' table."];
radiostation_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Radio Station' table."];

radiostation_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Radio Station' table."];
radiostation_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Radio Station' table."];
radiostation_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Radio Station' table."];
radiostation_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Radio Station' table, regardless of their owner."];

radiostation_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Radio Station' table."];
radiostation_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Radio Station' table."];
radiostation_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Radio Station' table."];
radiostation_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Radio Station' table."];

// country table
country_addTip=["",spacer+"This option allows all members of the group to add records to the 'Country' table. A member who adds a record to the table becomes the 'owner' of that record."];

country_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Country' table."];
country_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Country' table."];
country_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Country' table."];
country_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Country' table."];

country_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Country' table."];
country_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Country' table."];
country_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Country' table."];
country_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Country' table, regardless of their owner."];

country_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Country' table."];
country_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Country' table."];
country_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Country' table."];
country_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Country' table."];

// song table
song_addTip=["",spacer+"This option allows all members of the group to add records to the 'Song' table. A member who adds a record to the table becomes the 'owner' of that record."];

song_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Song' table."];
song_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Song' table."];
song_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Song' table."];
song_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Song' table."];

song_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Song' table."];
song_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Song' table."];
song_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Song' table."];
song_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Song' table, regardless of their owner."];

song_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Song' table."];
song_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Song' table."];
song_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Song' table."];
song_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Song' table."];

// radiosong table
radiosong_addTip=["",spacer+"This option allows all members of the group to add records to the 'Song by Station' table. A member who adds a record to the table becomes the 'owner' of that record."];

radiosong_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Song by Station' table."];
radiosong_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Song by Station' table."];
radiosong_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Song by Station' table."];
radiosong_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Song by Station' table."];

radiosong_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Song by Station' table."];
radiosong_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Song by Station' table."];
radiosong_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Song by Station' table."];
radiosong_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Song by Station' table, regardless of their owner."];

radiosong_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Song by Station' table."];
radiosong_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Song by Station' table."];
radiosong_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Song by Station' table."];
radiosong_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Song by Station' table."];

/*
	Style syntax:
	-------------
	[TitleColor,TextColor,TitleBgColor,TextBgColor,TitleBgImag,TextBgImag,TitleTextAlign,
	TextTextAlign,TitleFontFace,TextFontFace, TipPosition, StickyStyle, TitleFontSize,
	TextFontSize, Width, Height, BorderSize, PadTextArea, CoordinateX , CoordinateY,
	TransitionNumber, TransitionDuration, TransparencyLevel ,ShadowType, ShadowColor]

*/

toolTipStyle=["white","#00008B","#000099","#E6E6FA","","images/helpBg.gif","","","","\"Trebuchet MS\", sans-serif","","","","3",400,"",1,2,10,10,51,1,0,"",""];

applyCssFilter();
