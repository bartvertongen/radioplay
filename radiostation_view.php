<?php
// This script and data application were generated by AppGini 5.60
// Download AppGini for free from https://bigprof.com/appgini/download/

	$currDir=dirname(__FILE__);
	include("$currDir/defaultLang.php");
	include("$currDir/language.php");
	include("$currDir/lib.php");
	@include("$currDir/hooks/radiostation.php");
	include("$currDir/radiostation_dml.php");

	// mm: can the current member access this page?
	$perm=getTablePermissions('radiostation');
	if(!$perm[0]){
		echo error_message($Translation['tableAccessDenied'], false);
		echo '<script>setTimeout("window.location=\'index.php?signOut=1\'", 2000);</script>';
		exit;
	}

	$x = new DataList;
	$x->TableName = "radiostation";

	// Fields that can be displayed in the table view
	$x->QueryFieldsTV = array(   
		"`radiostation`.`RDO_ID`" => "RDO_ID",
		"`radiostation`.`RDO_ShortName`" => "RDO_ShortName",
		"`radiostation`.`RDO_LongName`" => "RDO_LongName",
		"`radiostation`.`RDO_Website`" => "RDO_Website",
		"`radiostation`.`RDO_Logo`" => "RDO_Logo",
		"IF(    CHAR_LENGTH(`country1`.`CTR_Name`), CONCAT_WS('',   `country1`.`CTR_Name`), '') /* Country */" => "RDO_CTR_Code"
	);
	// mapping incoming sort by requests to actual query fields
	$x->SortFields = array(   
		1 => '`radiostation`.`RDO_ID`',
		2 => 2,
		3 => 3,
		4 => 4,
		5 => 5,
		6 => 6
	);

	// Fields that can be displayed in the csv file
	$x->QueryFieldsCSV = array(   
		"`radiostation`.`RDO_ID`" => "RDO_ID",
		"`radiostation`.`RDO_ShortName`" => "RDO_ShortName",
		"`radiostation`.`RDO_LongName`" => "RDO_LongName",
		"`radiostation`.`RDO_Website`" => "RDO_Website",
		"`radiostation`.`RDO_Logo`" => "RDO_Logo",
		"IF(    CHAR_LENGTH(`country1`.`CTR_Name`), CONCAT_WS('',   `country1`.`CTR_Name`), '') /* Country */" => "RDO_CTR_Code"
	);
	// Fields that can be filtered
	$x->QueryFieldsFilters = array(   
		"`radiostation`.`RDO_ID`" => "ID",
		"`radiostation`.`RDO_ShortName`" => "Short Name",
		"`radiostation`.`RDO_LongName`" => "Name",
		"`radiostation`.`RDO_Website`" => "Website",
		"IF(    CHAR_LENGTH(`country1`.`CTR_Name`), CONCAT_WS('',   `country1`.`CTR_Name`), '') /* Country */" => "Country"
	);

	// Fields that can be quick searched
	$x->QueryFieldsQS = array(   
		"`radiostation`.`RDO_ID`" => "RDO_ID",
		"`radiostation`.`RDO_ShortName`" => "RDO_ShortName",
		"`radiostation`.`RDO_LongName`" => "RDO_LongName",
		"`radiostation`.`RDO_Website`" => "RDO_Website",
		"IF(    CHAR_LENGTH(`country1`.`CTR_Name`), CONCAT_WS('',   `country1`.`CTR_Name`), '') /* Country */" => "RDO_CTR_Code"
	);

	// Lookup fields that can be used as filterers
	$x->filterers = array(  'RDO_CTR_Code' => 'Country');

	$x->QueryFrom = "`radiostation` LEFT JOIN `country` as country1 ON `country1`.`CTR_Code`=`radiostation`.`RDO_CTR_Code` ";
	$x->QueryWhere = '';
	$x->QueryOrder = '';

	$x->AllowSelection = 1;
	$x->HideTableView = ($perm[2]==0 ? 1 : 0);
	$x->AllowDelete = $perm[4];
	$x->AllowMassDelete = false;
	$x->AllowInsert = $perm[1];
	$x->AllowUpdate = $perm[3];
	$x->SeparateDV = 1;
	$x->AllowDeleteOfParents = 0;
	$x->AllowFilters = 0;
	$x->AllowSavingFilters = 0;
	$x->AllowSorting = 1;
	$x->AllowNavigation = 1;
	$x->AllowPrinting = 0;
	$x->AllowCSV = 1;
	$x->RecordsPerPage = 10;
	$x->QuickSearch = 1;
	$x->QuickSearchText = $Translation["quick search"];
	$x->ScriptFileName = "radiostation_view.php";
	$x->RedirectAfterInsert = "radiostation_view.php?SelectedID=#ID#";
	$x->TableTitle = "Radio Station";
	$x->TableIcon = "table.gif";
	$x->PrimaryKey = "`radiostation`.`RDO_ID`";

	$x->ColWidth   = array(  50, 150, 150, 150, 150);
	$x->ColCaption = array("Short Name", "Name", "Website", "Logo", "Country");
	$x->ColFieldName = array('RDO_ShortName', 'RDO_LongName', 'RDO_Website', 'RDO_Logo', 'RDO_CTR_Code');
	$x->ColNumber  = array(2, 3, 4, 5, 6);

	$x->Template = 'templates/radiostation_templateTV.html';
	$x->SelectedTemplate = 'templates/radiostation_templateTVS.html';
	$x->ShowTableHeader = 1;
	$x->ShowRecordSlots = 0;
	$x->TVClasses = "";
	$x->DVClasses = "";
	$x->HighlightColor = '#FFF0C2';

	// mm: build the query based on current member's permissions
	$DisplayRecords = $_REQUEST['DisplayRecords'];
	if(!in_array($DisplayRecords, array('user', 'group'))){ $DisplayRecords = 'all'; }
	if($perm[2]==1 || ($perm[2]>1 && $DisplayRecords=='user' && !$_REQUEST['NoFilter_x'])){ // view owner only
		$x->QueryFrom.=', membership_userrecords';
		$x->QueryWhere="where `radiostation`.`RDO_ID`=membership_userrecords.pkValue and membership_userrecords.tableName='radiostation' and lcase(membership_userrecords.memberID)='".getLoggedMemberID()."'";
	}elseif($perm[2]==2 || ($perm[2]>2 && $DisplayRecords=='group' && !$_REQUEST['NoFilter_x'])){ // view group only
		$x->QueryFrom.=', membership_userrecords';
		$x->QueryWhere="where `radiostation`.`RDO_ID`=membership_userrecords.pkValue and membership_userrecords.tableName='radiostation' and membership_userrecords.groupID='".getLoggedGroupID()."'";
	}elseif($perm[2]==3){ // view all
		// no further action
	}elseif($perm[2]==0){ // view none
		$x->QueryFields = array("Not enough permissions" => "NEP");
		$x->QueryFrom = '`radiostation`';
		$x->QueryWhere = '';
		$x->DefaultSortField = '';
	}
	// hook: radiostation_init
	$render=TRUE;
	if(function_exists('radiostation_init')){
		$args=array();
		$render=radiostation_init($x, getMemberInfo(), $args);
	}

	if($render) $x->Render();

	// hook: radiostation_header
	$headerCode='';
	if(function_exists('radiostation_header')){
		$args=array();
		$headerCode=radiostation_header($x->ContentType, getMemberInfo(), $args);
	}  
	if(!$headerCode){
		include_once("$currDir/header.php"); 
	}else{
		ob_start(); include_once("$currDir/header.php"); $dHeader=ob_get_contents(); ob_end_clean();
		echo str_replace('<%%HEADER%%>', $dHeader, $headerCode);
	}

	echo $x->HTML;
	// hook: radiostation_footer
	$footerCode='';
	if(function_exists('radiostation_footer')){
		$args=array();
		$footerCode=radiostation_footer($x->ContentType, getMemberInfo(), $args);
	}  
	if(!$footerCode){
		include_once("$currDir/footer.php"); 
	}else{
		ob_start(); include_once("$currDir/footer.php"); $dFooter=ob_get_contents(); ob_end_clean();
		echo str_replace('<%%FOOTER%%>', $dFooter, $footerCode);
	}
?>