<?php

// Data functions (insert, update, delete, form) for table radiosong


function radiosong_insert()
{
	global $Translation;

	// mm: can member insert record?
	$arrPerm=getTablePermissions('radiosong');
	if(!$arrPerm[1])
	{
		return false;
	}
	$data['ROS_SNG_ID'] = makeSafe($_REQUEST['ROS_SNG_ID']);
	if($data['ROS_SNG_ID'] == empty_lookup_value)
		{ $data['ROS_SNG_ID'] = ''; }
		
	$data['ROS_RDO_ID'] = makeSafe($_REQUEST['ROS_RDO_ID']);
	if($data['ROS_RDO_ID'] == empty_lookup_value)
		{ $data['ROS_RDO_ID'] = ''; }
		
	$data['ROS_START'] = makeSafe($_REQUEST['ROS_START']);
	if($data['ROS_START'] == empty_lookup_value)
		{ $data['ROS_START'] = ''; }

	$data['ROS_END'] = makeSafe($_REQUEST['ROS_END']);
	if($data['ROS_END'] == empty_lookup_value)
		{ $data['ROS_END'] = ''; }

	if($data['ROS_SNG_ID']== ''){
		echo StyleSheet() . "\n\n<div class=\"alert alert-danger\">" . $Translation['error:'] . " 'Song': " . $Translation['field not null'] . '<br><br>';
		echo '<a href="" onclick="history.go(-1); return false;">'.$Translation['< back'].'</a></div>';
		exit;
	}
	if($data['ROS_RDO_ID']== ''){
		echo StyleSheet() . "\n\n<div class=\"alert alert-danger\">" . $Translation['error:'] . " 'Radio Station': " . $Translation['field not null'] . '<br><br>';
		echo '<a href="" onclick="history.go(-1); return false;">'.$Translation['< back'].'</a></div>';
		exit;
	}
	if($data['ROS_START']== ''){
		echo StyleSheet() . "\n\n<div class=\"alert alert-danger\">" . $Translation['error:'] . " 'Start Time': " . $Translation['field not null'] . '<br><br>';
		echo '<a href="" onclick="history.go(-1); return false;">'.$Translation['< back'].'</a></div>';
		exit;
	}
	if($data['ROS_END']== '')
	{
		echo StyleSheet() . "\n\n<div class=\"alert alert-danger\">" . $Translation['error:'] . " 'End Time': " . $Translation['field not null'] . '<br><br>';
		echo '<a href="" onclick="history.go(-1); return false;">'.$Translation['< back'].'</a></div>';
		exit;
	}

	// hook: radiosong_before_insert
	if (function_exists('radiosong_before_insert'))
	{
		$args=array();
		if(!radiosong_before_insert($data, getMemberInfo(), $args))
			{ return false; }
	}

	$o = array('silentErrors' => true);
	$strROS_SNG_ID = 'NULL';
	if ($data['ROS_SNG_ID'] !== '' && $data['ROS_SNG_ID'] !== NULL)
		$strROS_SNG_ID = $data['ROS_SNG_ID'];
	$strSql = 'insert into radiosong set ROS_SNG_ID='.$strROS_SNG_ID;
	
	$strROS_RDO_ID = 'NULL';
	if ($data['ROS_RDO_ID'] !== '' && $data['ROS_RDO_ID'] !== NULL)
		$strROS_RDO_ID = $data['ROS_RDO_ID'];
	$strSql .= ', ROS_RDO_ID='.$strROS_RDO_ID;
	
	$strROS_START = 'NULL';
	if ($data['ROS_START'] !== '' && $data['ROS_START'] !== NULL)
		$strROS_START = "STR_TO_DATE('".$data['ROS_START']."', '%e/%c/%Y %l:%i %p')";
	$strSql .= ', ROS_START='.$strROS_START;
	
	$strROS_END = 'NULL';
	if ($data['ROS_END'] !== '' && $data['ROS_END'] !== NULL)
		$strROS_END = "STR_TO_DATE('".$data['ROS_END']."', '%e/%c/%Y %l:%i %p')";
	$strSql .= ', ROS_END='.$strROS_END;
	
	sql($strSql, $o);
	if($o['error']!='')
	{
		echo $o['error'];
		echo "<a href=\"radiosong_view.php?addNew_x=1\">{$Translation['< back']}</a>";
		exit;
	}

	$recID = db_insert_id(db_link());

	// hook: radiosong_after_insert
	if (function_exists('radiosong_after_insert'))
	{
		$res = sql("select * from `radiosong` where `ROS_ID`='" .
				makeSafe($recID, false) . "' limit 1", $eo);
		if ($row = db_fetch_assoc($res)){
			$data = array_map('makeSafe', $row);
		}
		$data['selectedID'] = makeSafe($recID, false);
		$args=array();
		if(!radiosong_after_insert($data, getMemberInfo(), $args)){ return $recID; }
	}

	// mm: save ownership data
	sql("insert ignore into membership_userrecords set tableName='radiosong', pkValue='" . makeSafe($recID, false) . "', memberID='" . makeSafe(getLoggedMemberID(), false) . "', dateAdded='" . time() . "', dateUpdated='" . time() . "', groupID='" . getLoggedGroupID() . "'", $eo);

	return $recID;
}

function radiosong_delete($selected_id, $AllowDeleteOfParents=false, $skipChecks=false){
	// insure referential integrity ...
	global $Translation;
	$selected_id=makeSafe($selected_id);

	// mm: can member delete record?
	$arrPerm=getTablePermissions('radiosong');
	$ownerGroupID=sqlValue("select groupID from membership_userrecords where tableName='radiosong' and pkValue='$selected_id'");
	$ownerMemberID=sqlValue("select lcase(memberID) from membership_userrecords where tableName='radiosong' and pkValue='$selected_id'");
	if(($arrPerm[4]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[4]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[4]==3){ // allow delete?
		// delete allowed, so continue ...
	}else{
		return $Translation['You don\'t have enough permissions to delete this record'];
	}

	// hook: radiosong_before_delete
	if(function_exists('radiosong_before_delete')){
		$args=array();
		if(!radiosong_before_delete($selected_id, $skipChecks, getMemberInfo(), $args))
			return $Translation['Couldn\'t delete this record'];
	}

	sql("delete from `radiosong` where `ROS_ID`='$selected_id'", $eo);

	// hook: radiosong_after_delete
	if(function_exists('radiosong_after_delete')){
		$args=array();
		radiosong_after_delete($selected_id, getMemberInfo(), $args);
	}

	// mm: delete ownership data
	sql("delete from membership_userrecords where tableName='radiosong' and pkValue='$selected_id'", $eo);
}

function radiosong_update($selected_id){
	global $Translation;

	// mm: can member edit record?
	$arrPerm=getTablePermissions('radiosong');
	$ownerGroupID=sqlValue("select groupID from membership_userrecords where tableName='radiosong' and pkValue='".makeSafe($selected_id)."'");
	$ownerMemberID=sqlValue("select lcase(memberID) from membership_userrecords where tableName='radiosong' and pkValue='".makeSafe($selected_id)."'");
	if(($arrPerm[3]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[3]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[3]==3){ // allow update?
		// update allowed, so continue ...
	}else{
		return false;
	}

	$data['ROS_SNG_ID'] = makeSafe($_REQUEST['ROS_SNG_ID']);
		if($data['ROS_SNG_ID'] == empty_lookup_value){ $data['ROS_SNG_ID'] = ''; }
	if($data['ROS_SNG_ID']==''){
		echo StyleSheet() . "\n\n<div class=\"alert alert-danger\">{$Translation['error:']} 'Song': {$Translation['field not null']}<br><br>";
		echo '<a href="" onclick="history.go(-1); return false;">'.$Translation['< back'].'</a></div>';
		exit;
	}
	$data['ROS_RDO_ID'] = makeSafe($_REQUEST['ROS_RDO_ID']);
		if($data['ROS_RDO_ID'] == empty_lookup_value){ $data['ROS_RDO_ID'] = ''; }
	if($data['ROS_RDO_ID']==''){
		echo StyleSheet() . "\n\n<div class=\"alert alert-danger\">{$Translation['error:']} 'Radio Station': {$Translation['field not null']}<br><br>";
		echo '<a href="" onclick="history.go(-1); return false;">'.$Translation['< back'].'</a></div>';
		exit;
	}
	$data['ROS_START'] = makeSafe($_REQUEST['ROS_START']);
		if($data['ROS_START'] == empty_lookup_value){ $data['ROS_START'] = ''; }
	if($data['ROS_START']==''){
		echo StyleSheet() . "\n\n<div class=\"alert alert-danger\">{$Translation['error:']} 'Start Time': {$Translation['field not null']}<br><br>";
		echo '<a href="" onclick="history.go(-1); return false;">'.$Translation['< back'].'</a></div>';
		exit;
	}
	$data['ROS_END'] = makeSafe($_REQUEST['ROS_END']);
		if($data['ROS_END'] == empty_lookup_value){ $data['ROS_END'] = ''; }
	if($data['ROS_END']==''){
		echo StyleSheet() . "\n\n<div class=\"alert alert-danger\">{$Translation['error:']} 'End Time': {$Translation['field not null']}<br><br>";
		echo '<a href="" onclick="history.go(-1); return false;">'.$Translation['< back'].'</a></div>';
		exit;
	}
	$data['selectedID']=makeSafe($selected_id);

	// hook: radiosong_before_update
	if(function_exists('radiosong_before_update')){
		$args=array();
		if(!radiosong_before_update($data, getMemberInfo(), $args)){ return false; }
	}

	$o=array('silentErrors' => true);
	sql('update `radiosong` set       `ROS_SNG_ID`=' . (($data['ROS_SNG_ID'] !== '' && $data['ROS_SNG_ID'] !== NULL) ? "'{$data['ROS_SNG_ID']}'" : 'NULL') . ', `ROS_RDO_ID`=' . (($data['ROS_RDO_ID'] !== '' && $data['ROS_RDO_ID'] !== NULL) ? "'{$data['ROS_RDO_ID']}'" : 'NULL') . ', `ROS_START`=' . (($data['ROS_START'] !== '' && $data['ROS_START'] !== NULL) ? "'{$data['ROS_START']}'" : 'NULL') . ', `ROS_END`=' . (($data['ROS_END'] !== '' && $data['ROS_END'] !== NULL) ? "'{$data['ROS_END']}'" : 'NULL') . " where `ROS_ID`='".makeSafe($selected_id)."'", $o);
	if($o['error']!=''){
		echo $o['error'];
		echo '<a href="radiosong_view.php?SelectedID='.urlencode($selected_id)."\">{$Translation['< back']}</a>";
		exit;
	}


	// hook: radiosong_after_update
	if(function_exists('radiosong_after_update')){
		$res = sql("SELECT * FROM `radiosong` WHERE `ROS_ID`='{$data['selectedID']}' LIMIT 1", $eo);
		if($row = db_fetch_assoc($res)){
			$data = array_map('makeSafe', $row);
		}
		$data['selectedID'] = $data['ROS_ID'];
		$args = array();
		if(!radiosong_after_update($data, getMemberInfo(), $args)){ return; }
	}

	// mm: update ownership data
	sql("update membership_userrecords set dateUpdated='".time()."' where tableName='radiosong' and pkValue='".makeSafe($selected_id)."'", $eo);

}

function radiosong_form($selected_id = '', $AllowUpdate = 1, $AllowInsert = 1, $AllowDelete = 1, $ShowCancel = 0){
	// function to return an editable form for a table records
	// and fill it with data of record whose ID is $selected_id. If $selected_id
	// is empty, an empty form is shown, with only an 'Add New'
	// button displayed.

	global $Translation;

	// mm: get table permissions
	$arrPerm=getTablePermissions('radiosong');
	if(!$arrPerm[1] && $selected_id==''){ return ''; }
	$AllowInsert = ($arrPerm[1] ? true : false);
	// print preview?
	$dvprint = false;
	if($selected_id && $_REQUEST['dvprint_x'] != ''){
		$dvprint = true;
	}

	$filterer_ROS_SNG_ID = thisOr(undo_magic_quotes($_REQUEST['filterer_ROS_SNG_ID']), '');
	$filterer_ROS_RDO_ID = thisOr(undo_magic_quotes($_REQUEST['filterer_ROS_RDO_ID']), '');

	// populate filterers, starting from children to grand-parents

	// unique random identifier
	$rnd1 = ($dvprint ? rand(1000000, 9999999) : '');
	// combobox: ROS_SNG_ID
	$combo_ROS_SNG_ID = new DataCombo;
	// combobox: ROS_RDO_ID
	$combo_ROS_RDO_ID = new DataCombo;

	if($selected_id){
		// mm: check member permissions
		if(!$arrPerm[2]){
			return "";
		}
		// mm: who is the owner?
		$ownerGroupID=sqlValue("select groupID from membership_userrecords where tableName='radiosong' and pkValue='".makeSafe($selected_id)."'");
		$ownerMemberID=sqlValue("select lcase(memberID) from membership_userrecords where tableName='radiosong' and pkValue='".makeSafe($selected_id)."'");
		if($arrPerm[2]==1 && getLoggedMemberID()!=$ownerMemberID){
			return "";
		}
		if($arrPerm[2]==2 && getLoggedGroupID()!=$ownerGroupID){
			return "";
		}

		// can edit?
		if(($arrPerm[3]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[3]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[3]==3){
			$AllowUpdate=1;
		}else{
			$AllowUpdate=0;
		}

		$res = sql("select * from `radiosong` where `ROS_ID`='".makeSafe($selected_id)."'", $eo);
		if(!($row = db_fetch_array($res))){
			return error_message($Translation['No records found'], 'radiosong_view.php', false);
		}
		$urow = $row; /* unsanitized data */
		$hc = new CI_Input();
		$row = $hc->xss_clean($row); /* sanitize data */
		$combo_ROS_SNG_ID->SelectedData = $row['ROS_SNG_ID'];
		$combo_ROS_RDO_ID->SelectedData = $row['ROS_RDO_ID'];
	}else{
		$combo_ROS_SNG_ID->SelectedData = $filterer_ROS_SNG_ID;
		$combo_ROS_RDO_ID->SelectedData = $filterer_ROS_RDO_ID;
	}
	$combo_ROS_SNG_ID->HTML = '<span id="ROS_SNG_ID-container' . $rnd1 . '"></span><input type="hidden" name="ROS_SNG_ID" id="ROS_SNG_ID' . $rnd1 . '" value="' . html_attr($combo_ROS_SNG_ID->SelectedData) . '">';
	$combo_ROS_SNG_ID->MatchText = '<span id="ROS_SNG_ID-container-readonly' . $rnd1 . '"></span><input type="hidden" name="ROS_SNG_ID" id="ROS_SNG_ID' . $rnd1 . '" value="' . html_attr($combo_ROS_SNG_ID->SelectedData) . '">';
	$combo_ROS_RDO_ID->HTML = '<span id="ROS_RDO_ID-container' . $rnd1 . '"></span><input type="hidden" name="ROS_RDO_ID" id="ROS_RDO_ID' . $rnd1 . '" value="' . html_attr($combo_ROS_RDO_ID->SelectedData) . '">';
	$combo_ROS_RDO_ID->MatchText = '<span id="ROS_RDO_ID-container-readonly' . $rnd1 . '"></span><input type="hidden" name="ROS_RDO_ID" id="ROS_RDO_ID' . $rnd1 . '" value="' . html_attr($combo_ROS_RDO_ID->SelectedData) . '">';

	ob_start();
	?>

	<script>
		// initial lookup values
		var current_ROS_SNG_ID__RAND__ = { text: "", value: "<?php echo addslashes($selected_id ? $urow['ROS_SNG_ID'] : $filterer_ROS_SNG_ID); ?>"};
		var current_ROS_RDO_ID__RAND__ = { text: "", value: "<?php echo addslashes($selected_id ? $urow['ROS_RDO_ID'] : $filterer_ROS_RDO_ID); ?>"};

		jQuery(function() {
			if(typeof(ROS_SNG_ID_reload__RAND__) == 'function') ROS_SNG_ID_reload__RAND__();
			if(typeof(ROS_RDO_ID_reload__RAND__) == 'function') ROS_RDO_ID_reload__RAND__();
		});
		function ROS_SNG_ID_reload__RAND__(){
		<?php if(($AllowUpdate || $AllowInsert) && !$dvprint){ ?>

			jQuery("#ROS_SNG_ID-container__RAND__").select2({
				/* initial default value */
				initSelection: function(e, c){
					jQuery.ajax({
						url: 'ajax_combo.php',
						dataType: 'json',
						data: { id: current_ROS_SNG_ID__RAND__.value, t: 'radiosong', f: 'ROS_SNG_ID' }
					}).done(function(resp){
						c({
							id: resp.results[0].id,
							text: resp.results[0].text
						});
						$j('[name="ROS_SNG_ID"]').val(resp.results[0].id);
						$j('[id=ROS_SNG_ID-container-readonly__RAND__]').html('<span id="ROS_SNG_ID-match-text">' + resp.results[0].text + '</span>');
						if(resp.results[0].id == '<?php echo empty_lookup_value; ?>'){ $j('.btn[id=song_view_parent]').hide(); }else{ $j('.btn[id=song_view_parent]').show(); }


						if(typeof(ROS_SNG_ID_update_autofills__RAND__) == 'function') ROS_SNG_ID_update_autofills__RAND__();
					});
				},
				width: ($j('fieldset .col-xs-11').width() - select2_max_width_decrement()) + 'px',
				formatNoMatches: function(term){ return '<?php echo addslashes($Translation['No matches found!']); ?>'; },
				minimumResultsForSearch: 10,
				loadMorePadding: 200,
				ajax: {
					url: 'ajax_combo.php',
					dataType: 'json',
					cache: true,
					data: function(term, page){ return { s: term, p: page, t: 'radiosong', f: 'ROS_SNG_ID' }; },
					results: function(resp, page){ return resp; }
				},
				escapeMarkup: function(str){ return str; }
			}).on('change', function(e){
				current_ROS_SNG_ID__RAND__.value = e.added.id;
				current_ROS_SNG_ID__RAND__.text = e.added.text;
				$j('[name="ROS_SNG_ID"]').val(e.added.id);
				if(e.added.id == '<?php echo empty_lookup_value; ?>'){ $j('.btn[id=song_view_parent]').hide(); }else{ $j('.btn[id=song_view_parent]').show(); }


				if(typeof(ROS_SNG_ID_update_autofills__RAND__) == 'function') ROS_SNG_ID_update_autofills__RAND__();
			});

			if(!$j("#ROS_SNG_ID-container__RAND__").length){
				$j.ajax({
					url: 'ajax_combo.php',
					dataType: 'json',
					data: { id: current_ROS_SNG_ID__RAND__.value, t: 'radiosong', f: 'ROS_SNG_ID' }
				}).done(function(resp){
					$j('[name="ROS_SNG_ID"]').val(resp.results[0].id);
					$j('[id=ROS_SNG_ID-container-readonly__RAND__]').html('<span id="ROS_SNG_ID-match-text">' + resp.results[0].text + '</span>');
					if(resp.results[0].id == '<?php echo empty_lookup_value; ?>'){ $j('.btn[id=song_view_parent]').hide(); }else{ $j('.btn[id=song_view_parent]').show(); }

					if(typeof(ROS_SNG_ID_update_autofills__RAND__) == 'function') ROS_SNG_ID_update_autofills__RAND__();
				});
			}

		<?php }else{ ?>

			$j.ajax({
				url: 'ajax_combo.php',
				dataType: 'json',
				data: { id: current_ROS_SNG_ID__RAND__.value, t: 'radiosong', f: 'ROS_SNG_ID' }
			}).done(function(resp){
				$j('[id=ROS_SNG_ID-container__RAND__], [id=ROS_SNG_ID-container-readonly__RAND__]').html('<span id="ROS_SNG_ID-match-text">' + resp.results[0].text + '</span>');
				if(resp.results[0].id == '<?php echo empty_lookup_value; ?>'){ $j('.btn[id=song_view_parent]').hide(); }else{ $j('.btn[id=song_view_parent]').show(); }

				if(typeof(ROS_SNG_ID_update_autofills__RAND__) == 'function') ROS_SNG_ID_update_autofills__RAND__();
			});
		<?php } ?>

		}
		function ROS_RDO_ID_reload__RAND__(){
		<?php if(($AllowUpdate || $AllowInsert) && !$dvprint){ ?>

			jQuery("#ROS_RDO_ID-container__RAND__").select2({
				/* initial default value */
				initSelection: function(e, c){
					jQuery.ajax({
						url: 'ajax_combo.php',
						dataType: 'json',
						data: { id: current_ROS_RDO_ID__RAND__.value, t: 'radiosong', f: 'ROS_RDO_ID' }
					}).done(function(resp){
						c({
							id: resp.results[0].id,
							text: resp.results[0].text
						});
						$j('[name="ROS_RDO_ID"]').val(resp.results[0].id);
						$j('[id=ROS_RDO_ID-container-readonly__RAND__]').html('<span id="ROS_RDO_ID-match-text">' + resp.results[0].text + '</span>');
						if(resp.results[0].id == '<?php echo empty_lookup_value; ?>'){ $j('.btn[id=radiostation_view_parent]').hide(); }else{ $j('.btn[id=radiostation_view_parent]').show(); }


						if(typeof(ROS_RDO_ID_update_autofills__RAND__) == 'function') ROS_RDO_ID_update_autofills__RAND__();
					});
				},
				width: ($j('fieldset .col-xs-11').width() - select2_max_width_decrement()) + 'px',
				formatNoMatches: function(term){ return '<?php echo addslashes($Translation['No matches found!']); ?>'; },
				minimumResultsForSearch: 10,
				loadMorePadding: 200,
				ajax: {
					url: 'ajax_combo.php',
					dataType: 'json',
					cache: true,
					data: function(term, page){ return { s: term, p: page, t: 'radiosong', f: 'ROS_RDO_ID' }; },
					results: function(resp, page){ return resp; }
				},
				escapeMarkup: function(str){ return str; }
			}).on('change', function(e){
				current_ROS_RDO_ID__RAND__.value = e.added.id;
				current_ROS_RDO_ID__RAND__.text = e.added.text;
				$j('[name="ROS_RDO_ID"]').val(e.added.id);
				if(e.added.id == '<?php echo empty_lookup_value; ?>'){ $j('.btn[id=radiostation_view_parent]').hide(); }else{ $j('.btn[id=radiostation_view_parent]').show(); }


				if(typeof(ROS_RDO_ID_update_autofills__RAND__) == 'function') ROS_RDO_ID_update_autofills__RAND__();
			});

			if(!$j("#ROS_RDO_ID-container__RAND__").length){
				$j.ajax({
					url: 'ajax_combo.php',
					dataType: 'json',
					data: { id: current_ROS_RDO_ID__RAND__.value, t: 'radiosong', f: 'ROS_RDO_ID' }
				}).done(function(resp){
					$j('[name="ROS_RDO_ID"]').val(resp.results[0].id);
					$j('[id=ROS_RDO_ID-container-readonly__RAND__]').html('<span id="ROS_RDO_ID-match-text">' + resp.results[0].text + '</span>');
					if(resp.results[0].id == '<?php echo empty_lookup_value; ?>'){ $j('.btn[id=radiostation_view_parent]').hide(); }else{ $j('.btn[id=radiostation_view_parent]').show(); }

					if(typeof(ROS_RDO_ID_update_autofills__RAND__) == 'function') ROS_RDO_ID_update_autofills__RAND__();
				});
			}

		<?php }else{ ?>

			$j.ajax({
				url: 'ajax_combo.php',
				dataType: 'json',
				data: { id: current_ROS_RDO_ID__RAND__.value, t: 'radiosong', f: 'ROS_RDO_ID' }
			}).done(function(resp){
				$j('[id=ROS_RDO_ID-container__RAND__], [id=ROS_RDO_ID-container-readonly__RAND__]').html('<span id="ROS_RDO_ID-match-text">' + resp.results[0].text + '</span>');
				if(resp.results[0].id == '<?php echo empty_lookup_value; ?>'){ $j('.btn[id=radiostation_view_parent]').hide(); }else{ $j('.btn[id=radiostation_view_parent]').show(); }

				if(typeof(ROS_RDO_ID_update_autofills__RAND__) == 'function') ROS_RDO_ID_update_autofills__RAND__();
			});
		<?php } ?>

		}
	</script>
	<?php

	$lookups = str_replace('__RAND__', $rnd1, ob_get_contents());
	ob_end_clean();


	// code for template based detail view forms

	// open the detail view template
	$templateCode = @file_get_contents('./templates/radiosong_templateDV.html');

	// process form title
	$templateCode = str_replace('<%%DETAIL_VIEW_TITLE%%>', 'Radiosong details', $templateCode);
	$templateCode = str_replace('<%%RND1%%>', $rnd1, $templateCode);
	$templateCode = str_replace('<%%EMBEDDED%%>', ($_REQUEST['Embedded'] ? 'Embedded=1' : ''), $templateCode);
	// process buttons
	if($arrPerm[1] && !$selected_id){ // allow insert and no record selected?
		if(!$selected_id) $templateCode=str_replace('<%%INSERT_BUTTON%%>', '<button type="submit" class="btn btn-success" id="insert" name="insert_x" value="1" onclick="return radiosong_validateData();"><i class="glyphicon glyphicon-plus-sign"></i> ' . $Translation['Save New'] . '</button>', $templateCode);
		$templateCode=str_replace('<%%INSERT_BUTTON%%>', '<button type="submit" class="btn btn-default" id="insert" name="insert_x" value="1" onclick="return radiosong_validateData();"><i class="glyphicon glyphicon-plus-sign"></i> ' . $Translation['Save As Copy'] . '</button>', $templateCode);
	}else{
		$templateCode=str_replace('<%%INSERT_BUTTON%%>', '', $templateCode);
	}

	// 'Back' button action
	if($_REQUEST['Embedded']){
		$backAction = 'window.parent.jQuery(\'.modal\').modal(\'hide\'); return false;';
	}else{
		$backAction = '$$(\'form\')[0].writeAttribute(\'novalidate\', \'novalidate\'); document.myform.reset(); return true;';
	}

	if($selected_id){
		if($AllowUpdate){
			$templateCode=str_replace('<%%UPDATE_BUTTON%%>', '<button type="submit" class="btn btn-success btn-lg" id="update" name="update_x" value="1" onclick="return radiosong_validateData();" title="' . html_attr($Translation['Save Changes']) . '"><i class="glyphicon glyphicon-ok"></i> ' . $Translation['Save Changes'] . '</button>', $templateCode);
		}else{
			$templateCode=str_replace('<%%UPDATE_BUTTON%%>', '', $templateCode);
		}
		if(($arrPerm[4]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[4]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[4]==3){ // allow delete?
			$templateCode=str_replace('<%%DELETE_BUTTON%%>', '<button type="submit" class="btn btn-danger" id="delete" name="delete_x" value="1" onclick="return confirm(\'' . $Translation['are you sure?'] . '\');" title="' . html_attr($Translation['Delete']) . '"><i class="glyphicon glyphicon-trash"></i> ' . $Translation['Delete'] . '</button>', $templateCode);
		}else{
			$templateCode=str_replace('<%%DELETE_BUTTON%%>', '', $templateCode);
		}
		$templateCode=str_replace('<%%DESELECT_BUTTON%%>', '<button type="submit" class="btn btn-default" id="deselect" name="deselect_x" value="1" onclick="' . $backAction . '" title="' . html_attr($Translation['Back']) . '"><i class="glyphicon glyphicon-chevron-left"></i> ' . $Translation['Back'] . '</button>', $templateCode);
	}else{
		$templateCode=str_replace('<%%UPDATE_BUTTON%%>', '', $templateCode);
		$templateCode=str_replace('<%%DELETE_BUTTON%%>', '', $templateCode);
		$templateCode=str_replace('<%%DESELECT_BUTTON%%>', ($ShowCancel ? '<button type="submit" class="btn btn-default" id="deselect" name="deselect_x" value="1" onclick="' . $backAction . '" title="' . html_attr($Translation['Back']) . '"><i class="glyphicon glyphicon-chevron-left"></i> ' . $Translation['Back'] . '</button>' : ''), $templateCode);
	}

	// set records to read only if user can't insert new records and can't edit current record
	if(($selected_id && !$AllowUpdate) || (!$selected_id && !$AllowInsert)){
		$jsReadOnly .= "\tjQuery('#ROS_SNG_ID').prop('disabled', true).css({ color: '#555', backgroundColor: '#fff' });\n";
		$jsReadOnly .= "\tjQuery('#ROS_SNG_ID_caption').prop('disabled', true).css({ color: '#555', backgroundColor: 'white' });\n";
		$jsReadOnly .= "\tjQuery('#ROS_RDO_ID').prop('disabled', true).css({ color: '#555', backgroundColor: '#fff' });\n";
		$jsReadOnly .= "\tjQuery('#ROS_RDO_ID_caption').prop('disabled', true).css({ color: '#555', backgroundColor: 'white' });\n";
		$jsReadOnly .= "\tjQuery('#ROS_START').replaceWith('<div class=\"form-control-static\" id=\"ROS_START\">' + (jQuery('#ROS_START').val() || '') + '</div>');\n";
		$jsReadOnly .= "\tjQuery('#ROS_END').replaceWith('<div class=\"form-control-static\" id=\"ROS_END\">' + (jQuery('#ROS_END').val() || '') + '</div>');\n";
		$jsReadOnly .= "\tjQuery('.select2-container').hide();\n";

		$noUploads = true;
	}elseif(($AllowInsert && !$selected_id) || ($AllowUpdate && $selected_id)){
		$jsEditable .= "\tjQuery('form').eq(0).data('already_changed', true);"; // temporarily disable form change handler
			$jsEditable .= "\tjQuery('form').eq(0).data('already_changed', false);"; // re-enable form change handler
	}

	// process combos
	$templateCode=str_replace('<%%COMBO(ROS_SNG_ID)%%>', $combo_ROS_SNG_ID->HTML, $templateCode);
	$templateCode=str_replace('<%%COMBOTEXT(ROS_SNG_ID)%%>', $combo_ROS_SNG_ID->MatchText, $templateCode);
	$templateCode=str_replace('<%%URLCOMBOTEXT(ROS_SNG_ID)%%>', urlencode($combo_ROS_SNG_ID->MatchText), $templateCode);
	$templateCode=str_replace('<%%COMBO(ROS_RDO_ID)%%>', $combo_ROS_RDO_ID->HTML, $templateCode);
	$templateCode=str_replace('<%%COMBOTEXT(ROS_RDO_ID)%%>', $combo_ROS_RDO_ID->MatchText, $templateCode);
	$templateCode=str_replace('<%%URLCOMBOTEXT(ROS_RDO_ID)%%>', urlencode($combo_ROS_RDO_ID->MatchText), $templateCode);

	/* lookup fields array: 'lookup field name' => array('parent table name', 'lookup field caption') */
	$lookup_fields = array(  'ROS_SNG_ID' => array('song', 'Song'), 'ROS_RDO_ID' => array('radiostation', 'Radio Station'));
	foreach($lookup_fields as $luf => $ptfc){
		$pt_perm = getTablePermissions($ptfc[0]);

		// process foreign key links
		if($pt_perm['view'] || $pt_perm['edit']){
			$templateCode = str_replace("<%%PLINK({$luf})%%>", '<button type="button" class="btn btn-default view_parent hspacer-md" id="' . $ptfc[0] . '_view_parent" title="' . html_attr($Translation['View'] . ' ' . $ptfc[1]) . '"><i class="glyphicon glyphicon-eye-open"></i></button>', $templateCode);
		}

		// if user has insert permission to parent table of a lookup field, put an add new button
		if($pt_perm['insert'] && !$_REQUEST['Embedded']){
			$templateCode = str_replace("<%%ADDNEW({$ptfc[0]})%%>", '<button type="button" class="btn btn-success add_new_parent hspacer-md" id="' . $ptfc[0] . '_add_new" title="' . html_attr($Translation['Add New'] . ' ' . $ptfc[1]) . '"><i class="glyphicon glyphicon-plus-sign"></i></button>', $templateCode);
		}
	}

	// process images
	$templateCode=str_replace('<%%UPLOADFILE(ROS_ID)%%>', '', $templateCode);
	$templateCode=str_replace('<%%UPLOADFILE(ROS_SNG_ID)%%>', '', $templateCode);
	$templateCode=str_replace('<%%UPLOADFILE(ROS_RDO_ID)%%>', '', $templateCode);
	$templateCode=str_replace('<%%UPLOADFILE(ROS_START)%%>', '', $templateCode);
	$templateCode=str_replace('<%%UPLOADFILE(ROS_END)%%>', '', $templateCode);

	// process values
	if($selected_id){
		$templateCode=str_replace('<%%VALUE(ROS_ID)%%>', html_attr($row['ROS_ID']), $templateCode);
		$templateCode=str_replace('<%%URLVALUE(ROS_ID)%%>', urlencode($urow['ROS_ID']), $templateCode);
		$templateCode=str_replace('<%%VALUE(ROS_SNG_ID)%%>', html_attr($row['ROS_SNG_ID']), $templateCode);
		$templateCode=str_replace('<%%URLVALUE(ROS_SNG_ID)%%>', urlencode($urow['ROS_SNG_ID']), $templateCode);
		$templateCode=str_replace('<%%VALUE(ROS_RDO_ID)%%>', html_attr($row['ROS_RDO_ID']), $templateCode);
		$templateCode=str_replace('<%%URLVALUE(ROS_RDO_ID)%%>', urlencode($urow['ROS_RDO_ID']), $templateCode);
		$templateCode=str_replace('<%%VALUE(ROS_START)%%>', html_attr($row['ROS_START']), $templateCode);
		$templateCode=str_replace('<%%URLVALUE(ROS_START)%%>', urlencode($urow['ROS_START']), $templateCode);
		$templateCode=str_replace('<%%VALUE(ROS_END)%%>', html_attr($row['ROS_END']), $templateCode);
		$templateCode=str_replace('<%%URLVALUE(ROS_END)%%>', urlencode($urow['ROS_END']), $templateCode);
	}else{
		$templateCode=str_replace('<%%VALUE(ROS_ID)%%>', '', $templateCode);
		$templateCode=str_replace('<%%URLVALUE(ROS_ID)%%>', urlencode(''), $templateCode);
		$templateCode=str_replace('<%%VALUE(ROS_SNG_ID)%%>', '', $templateCode);
		$templateCode=str_replace('<%%URLVALUE(ROS_SNG_ID)%%>', urlencode(''), $templateCode);
		$templateCode=str_replace('<%%VALUE(ROS_RDO_ID)%%>', '', $templateCode);
		$templateCode=str_replace('<%%URLVALUE(ROS_RDO_ID)%%>', urlencode(''), $templateCode);
		$templateCode=str_replace('<%%VALUE(ROS_START)%%>', '', $templateCode);
		$templateCode=str_replace('<%%URLVALUE(ROS_START)%%>', urlencode(''), $templateCode);
		$templateCode=str_replace('<%%VALUE(ROS_END)%%>', '', $templateCode);
		$templateCode=str_replace('<%%URLVALUE(ROS_END)%%>', urlencode(''), $templateCode);
	}

	// process translations
	foreach($Translation as $symbol=>$trans){
		$templateCode=str_replace("<%%TRANSLATION($symbol)%%>", $trans, $templateCode);
	}

	// clear scrap
	$templateCode=str_replace('<%%', '<!-- ', $templateCode);
	$templateCode=str_replace('%%>', ' -->', $templateCode);

	// hide links to inaccessible tables
	if($_REQUEST['dvprint_x'] == ''){
		$templateCode .= "\n\n<script>\$j(function(){\n";
		$arrTables = getTableList();
		foreach($arrTables as $name => $caption){
			$templateCode .= "\t\$j('#{$name}_link').removeClass('hidden');\n";
			$templateCode .= "\t\$j('#xs_{$name}_link').removeClass('hidden');\n";
		}

		$templateCode .= $jsReadOnly;
		$templateCode .= $jsEditable;

		if(!$selected_id){
		}

		$templateCode.="\n});</script>\n";
	}

	// ajaxed auto-fill fields
	$templateCode .= '<script>';
	$templateCode .= '$j(function() {';


	$templateCode.="});";
	$templateCode.="</script>";
	$templateCode .= $lookups;

	// handle enforced parent values for read-only lookup fields

	// don't include blank images in lightbox gallery
	$templateCode = preg_replace('/blank.gif" data-lightbox=".*?"/', 'blank.gif"', $templateCode);

	// don't display empty email links
	$templateCode=preg_replace('/<a .*?href="mailto:".*?<\/a>/', '', $templateCode);

	// hook: radiosong_dv
	if(function_exists('radiosong_dv')){
		$args=array();
		radiosong_dv(($selected_id ? $selected_id : FALSE), getMemberInfo(), $templateCode, $args);
	}

	return $templateCode;
}
?>