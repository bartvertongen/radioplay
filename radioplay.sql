-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2017 at 04:49 PM
-- Server version: 5.7.17
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `radioplay`
--

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `CTR_Code` char(2) NOT NULL,
  `CTR_Name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`CTR_Code`, `CTR_Name`) VALUES
('AF', 'Afghanistan'),
('AX', 'Aland Islands'),
('AL', 'Albania'),
('DZ', 'Algeria'),
('AS', 'American Samoa'),
('AD', 'Andorra'),
('AO', 'Angola'),
('AI', 'Anguilla'),
('AQ', 'Antartica'),
('AG', 'Antigua and Barbuda'),
('AR', 'Argentina'),
('AM', 'Armenia'),
('AW', 'Aruba'),
('AU', 'Australia'),
('AT', 'Austria'),
('AZ', 'Azerbaijan'),
('BS', 'Bahamas'),
('BH', 'Bahrain'),
('BD', 'Bangladesh'),
('BB', 'Barbados'),
('BY', 'Belarus'),
('BE', 'Belgium'),
('BZ', 'Belize'),
('BJ', 'Benin'),
('BM', 'Bermuda'),
('BT', 'Bhutan'),
('BO', 'Bolivia'),
('BA', 'Bosnia and Herzegovina'),
('BW', 'Botswana'),
('BV', 'Bouvet Island'),
('BR', 'Brazil'),
('IO', 'British Indian Ocean Territory'),
('VG', 'British Virgin Islands'),
('BN', 'Brunei Darussalam'),
('BG', 'Bulgaria'),
('BF', 'Burkina Faso'),
('BI', 'Burundi'),
('KH', 'Cambodia'),
('CM', 'Cameroon'),
('CA', 'Canada'),
('CV', 'Cape Verde'),
('KY', 'Cayman Islands'),
('CF', 'Central African Republic'),
('TD', 'Chad'),
('CL', 'Chile'),
('CN', 'China'),
('CX', 'Christmas Island'),
('CC', 'Cocos (Keeling) Islands'),
('CO', 'Colombia'),
('KM', 'Comoros'),
('CG', 'Congo (Brazzaville)'),
('CD', 'Congo, Democratic Republic of the'),
('CK', 'Cook Islands'),
('CR', 'Costa Rica'),
('CI', 'Côte d\'Ivoire'),
('HR', 'Croatia'),
('CU', 'Cuba'),
('CY', 'Cyprus'),
('CZ', 'Czech Republic'),
('DK', 'Denmark'),
('DJ', 'Djibouti'),
('DM', 'Dominica'),
('DO', 'Dominican Republic'),
('EC', 'Ecuador'),
('EG', 'Egypt'),
('SV', 'El Salvador'),
('GQ', 'Equatorial Guinea'),
('ER', 'Eritrea'),
('EE', 'Estonia'),
('ET', 'Ethiopia'),
('FK', 'Falkland Islands (Malvinas)'),
('FO', 'Faroe Islands'),
('FJ', 'Fiji'),
('FI', 'Finland'),
('FR', 'France'),
('GF', 'French Guiana'),
('PF', 'French Polynesia'),
('TF', 'French Southern Territories'),
('GA', 'Gabon'),
('GM', 'Gambia'),
('GE', 'Georgia'),
('DE', 'Germany'),
('GH', 'Ghana'),
('GI', 'Gibraltar'),
('GR', 'Greece'),
('GL', 'Greenland'),
('GD', 'Grenada'),
('GP', 'Guadeloupe'),
('GU', 'Guam'),
('GT', 'Guatemala'),
('GG', 'Guernsey'),
('GN', 'Guinea'),
('GW', 'Guinea-Bissau'),
('GY', 'Guyana'),
('HT', 'Haiti'),
('HM', 'Heard Island and Mcdonald Islands'),
('VA', 'Holy See (Vatican City State)'),
('HN', 'Honduras'),
('HK', 'Hong Kong, Special Administrative Region of China'),
('HU', 'Hungary'),
('IS', 'Iceland'),
('IN', 'India'),
('ID', 'Indonesia'),
('IR', 'Iran, Islamic Republic of'),
('IQ', 'Iraq'),
('IE', 'Ireland'),
('IM', 'Isle of Man'),
('IL', 'Israel'),
('IT', 'Italy'),
('JM', 'Jamaica'),
('JP', 'Japan'),
('JE', 'Jersey'),
('JO', 'Jordan'),
('KE', 'Kenya'),
('KI', 'Kiribati'),
('KP', 'Korea, Democratic People\'s Republic of'),
('KR', 'Korea, Republic of'),
('KW', 'Kuwait'),
('KG', 'Kyrgyzstan'),
('LA', 'Lao PDR'),
('LV', 'Latvia'),
('LB', 'Lebanon'),
('LS', 'Lesotho'),
('LR', 'Liberia'),
('LY', 'Libya'),
('LI', 'Liechtenstein'),
('LT', 'Lithuania'),
('LU', 'Luxenbourg'),
('MO', 'Macao, Special Administrative Region of China'),
('MK', 'Macedonia, Republic of'),
('MG', 'Madagascar'),
('MW', 'Malawi'),
('MY', 'Malaysia'),
('MV', 'Maldives'),
('ML', 'Mali'),
('MT', 'Malta'),
('MH', 'Marshall Islands'),
('MQ', 'Martinique'),
('MR', 'Mauritania'),
('MU', 'Mauritius'),
('YT', 'Mayotte'),
('MX', 'Mexico'),
('FM', 'Micronesia, Federated States of'),
('MD', 'Moldova'),
('MC', 'Monaco'),
('MN', 'Mongolia'),
('ME', 'Montenegro'),
('MS', 'Montserrat'),
('MA', 'Morocco'),
('MZ', 'Mozambique'),
('MM', 'Myamar'),
('NA', 'Namibia'),
('NR', 'Nauru'),
('NP', 'Nepal'),
('NL', 'Netherlands'),
('AN', 'Netherlands Antilles'),
('NC', 'New Caledonia'),
('NZ', 'New Zealand'),
('NI', 'Nicaragua'),
('NE', 'Niger'),
('NG', 'Nigeria'),
('NU', 'Niue'),
('NF', 'Norfolk Island'),
('MP', 'Northern Mariana Islands'),
('NO', 'Norway'),
('OM', 'Oman'),
('PK', 'Pakistan'),
('PW', 'Palau'),
('PS', 'Palestinian Territory, Occupied'),
('PA', 'Panama'),
('PG', 'Papua New Guinea'),
('PY', 'Paraguay'),
('PE', 'Peru'),
('PH', 'Philippines'),
('PN', 'Pitcairn'),
('PL', 'Poland'),
('PT', 'Portugal'),
('PR', 'Puerto Rico'),
('QA', 'Qatar'),
('RE', 'Réunion'),
('RO', 'Romania'),
('RU', 'Russian Federation'),
('RW', 'Rwanda'),
('SH', 'Saint Helena'),
('KN', 'Saint Kitts and Nevis'),
('LC', 'Saint Lucia'),
('PM', 'Saint Pierre and Miquelon'),
('VC', 'Saint Vincent and Grenadines'),
('BL', 'Saint-Barthélemy'),
('MF', 'Saint-Martin (French part)'),
('WS', 'Samoa'),
('SM', 'San Marino'),
('ST', 'Sao Tome and Principe'),
('SA', 'Saudi Arabia'),
('SN', 'Senegal'),
('RS', 'Serbia'),
('SC', 'Seychelles'),
('SL', 'Sierra Leone'),
('SG', 'Singapore'),
('SK', 'Slovakia'),
('SI', 'Slovenia'),
('SB', 'Solomon Islands'),
('SO', 'Somalia'),
('ZA', 'South Africa'),
('GS', 'South Georgia and the South Sandwich Islands'),
('SS', 'South Sudan'),
('ES', 'Spain'),
('LK', 'Sri Lanka'),
('SD', 'Sudan'),
('SR', 'Suriname'),
('SJ', 'Svalbard and Jan Mayen Islands'),
('SZ', 'Swaziland'),
('SE', 'Sweden'),
('CH', 'Switzerland'),
('SY', 'Syrian Arab Republic (Syria)'),
('TW', 'Taiwan, Republic of China'),
('TJ', 'Tajikistan'),
('TZ', 'Tanzania, United Republic of'),
('TH', 'Thailand'),
('TL', 'Timor-Leste'),
('TG', 'Togo'),
('TK', 'Tokelau'),
('TO', 'Tonga'),
('TT', 'Trinidad and Tobago'),
('TN', 'Tunisia'),
('TR', 'Turkey'),
('TM', 'Turkmenistan'),
('TC', 'Turks and Caicos Islands'),
('TV', 'Tuvalu'),
('UG', 'Uganda'),
('UA', 'Ukraine'),
('AE', 'United Arab Emirates'),
('GB', 'United Kingdom'),
('UM', 'United States Minor Outlying Islands'),
('US', 'United States of America'),
('UY', 'Uruguay'),
('UZ', 'Uzbekistan'),
('VU', 'Vanuatu'),
('VE', 'Venezuela (Bolivarian Republic of)'),
('VN', 'Viet Nam'),
('VI', 'Virgin Islands, US'),
('WF', 'Wallis and Futuna Islands'),
('EH', 'Western Sahara'),
('YE', 'Yemen'),
('ZM', 'Zambia'),
('ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `membership_grouppermissions`
--

CREATE TABLE `membership_grouppermissions` (
  `permissionID` int(10) UNSIGNED NOT NULL,
  `groupID` int(11) DEFAULT NULL,
  `tableName` varchar(100) DEFAULT NULL,
  `allowInsert` tinyint(4) DEFAULT NULL,
  `allowView` tinyint(4) NOT NULL DEFAULT '0',
  `allowEdit` tinyint(4) NOT NULL DEFAULT '0',
  `allowDelete` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `membership_grouppermissions`
--

INSERT INTO `membership_grouppermissions` (`permissionID`, `groupID`, `tableName`, `allowInsert`, `allowView`, `allowEdit`, `allowDelete`) VALUES
(1, 2, 'country', 1, 3, 3, 3),
(2, 2, 'radiostation', 1, 3, 3, 3),
(3, 2, 'song', 1, 3, 3, 3),
(4, 2, 'radiosong', 1, 3, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `membership_groups`
--

CREATE TABLE `membership_groups` (
  `groupID` int(10) UNSIGNED NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `description` text,
  `allowSignup` tinyint(4) DEFAULT NULL,
  `needsApproval` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `membership_groups`
--

INSERT INTO `membership_groups` (`groupID`, `name`, `description`, `allowSignup`, `needsApproval`) VALUES
(1, 'anonymous', 'Anonymous group created automatically on 2017-05-13', 0, 0),
(2, 'Admins', 'Admin group created automatically on 2017-05-13', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `membership_userpermissions`
--

CREATE TABLE `membership_userpermissions` (
  `permissionID` int(10) UNSIGNED NOT NULL,
  `memberID` varchar(20) NOT NULL,
  `tableName` varchar(100) DEFAULT NULL,
  `allowInsert` tinyint(4) DEFAULT NULL,
  `allowView` tinyint(4) NOT NULL DEFAULT '0',
  `allowEdit` tinyint(4) NOT NULL DEFAULT '0',
  `allowDelete` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `membership_userrecords`
--

CREATE TABLE `membership_userrecords` (
  `recID` bigint(20) UNSIGNED NOT NULL,
  `tableName` varchar(100) DEFAULT NULL,
  `pkValue` varchar(255) DEFAULT NULL,
  `memberID` varchar(20) DEFAULT NULL,
  `dateAdded` bigint(20) UNSIGNED DEFAULT NULL,
  `dateUpdated` bigint(20) UNSIGNED DEFAULT NULL,
  `groupID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `membership_userrecords`
--

INSERT INTO `membership_userrecords` (`recID`, `tableName`, `pkValue`, `memberID`, `dateAdded`, `dateUpdated`, `groupID`) VALUES
(1, 'radiostation', '1', 'admin', 1494690862, 1494690862, 2),
(2, 'radiostation', '2', 'admin', 1494690972, 1494690972, 2),
(3, 'country', 'AD', 'admin', 1494726238, 1494726238, 2),
(4, 'country', 'AE', 'admin', 1494726238, 1494726238, 2),
(5, 'country', 'AF', 'admin', 1494726238, 1494726238, 2),
(6, 'country', 'AG', 'admin', 1494726238, 1494726238, 2),
(7, 'country', 'AI', 'admin', 1494726238, 1494726238, 2),
(8, 'country', 'AL', 'admin', 1494726238, 1494726238, 2),
(9, 'country', 'AM', 'admin', 1494726238, 1494726238, 2),
(10, 'country', 'AN', 'admin', 1494726238, 1494726238, 2),
(11, 'country', 'AO', 'admin', 1494726238, 1494726238, 2),
(12, 'country', 'AQ', 'admin', 1494726238, 1494726238, 2),
(13, 'country', 'AR', 'admin', 1494726238, 1494726238, 2),
(14, 'country', 'AS', 'admin', 1494726238, 1494726238, 2),
(15, 'country', 'AT', 'admin', 1494726238, 1494726238, 2),
(16, 'country', 'AU', 'admin', 1494726238, 1494726238, 2),
(17, 'country', 'AW', 'admin', 1494726238, 1494726238, 2),
(18, 'country', 'AX', 'admin', 1494726238, 1494726238, 2),
(19, 'country', 'AZ', 'admin', 1494726238, 1494726238, 2),
(20, 'country', 'BA', 'admin', 1494726238, 1494726238, 2),
(21, 'country', 'BB', 'admin', 1494726238, 1494726238, 2),
(22, 'country', 'BD', 'admin', 1494726238, 1494726238, 2),
(23, 'country', 'BE', 'admin', 1494726238, 1494726238, 2),
(24, 'country', 'BF', 'admin', 1494726238, 1494726238, 2),
(25, 'country', 'BG', 'admin', 1494726238, 1494726238, 2),
(26, 'country', 'BH', 'admin', 1494726238, 1494726238, 2),
(27, 'country', 'BI', 'admin', 1494726238, 1494726238, 2),
(28, 'country', 'BJ', 'admin', 1494726238, 1494726238, 2),
(29, 'country', 'BL', 'admin', 1494726238, 1494726238, 2),
(30, 'country', 'BM', 'admin', 1494726238, 1494726238, 2),
(31, 'country', 'BN', 'admin', 1494726238, 1494726238, 2),
(32, 'country', 'BO', 'admin', 1494726238, 1494726238, 2),
(33, 'country', 'BR', 'admin', 1494726238, 1494726238, 2),
(34, 'country', 'BS', 'admin', 1494726238, 1494726238, 2),
(35, 'country', 'BT', 'admin', 1494726238, 1494726238, 2),
(36, 'country', 'BV', 'admin', 1494726238, 1494726238, 2),
(37, 'country', 'BW', 'admin', 1494726238, 1494726238, 2),
(38, 'country', 'BY', 'admin', 1494726238, 1494726238, 2),
(39, 'country', 'BZ', 'admin', 1494726238, 1494726238, 2),
(40, 'country', 'CA', 'admin', 1494726238, 1494726238, 2),
(41, 'country', 'CC', 'admin', 1494726238, 1494726238, 2),
(42, 'country', 'CD', 'admin', 1494726238, 1494726238, 2),
(43, 'country', 'CF', 'admin', 1494726238, 1494726238, 2),
(44, 'country', 'CG', 'admin', 1494726238, 1494726238, 2),
(45, 'country', 'CH', 'admin', 1494726238, 1494726238, 2),
(46, 'country', 'CI', 'admin', 1494726238, 1494726238, 2),
(47, 'country', 'CK', 'admin', 1494726238, 1494726238, 2),
(48, 'country', 'CL', 'admin', 1494726238, 1494726238, 2),
(49, 'country', 'CM', 'admin', 1494726238, 1494726238, 2),
(50, 'country', 'CN', 'admin', 1494726238, 1494726238, 2),
(51, 'country', 'CO', 'admin', 1494726238, 1494726238, 2),
(52, 'country', 'CR', 'admin', 1494726238, 1494726238, 2),
(53, 'country', 'CU', 'admin', 1494726238, 1494726238, 2),
(54, 'country', 'CV', 'admin', 1494726238, 1494726238, 2),
(55, 'country', 'CX', 'admin', 1494726238, 1494726238, 2),
(56, 'country', 'CY', 'admin', 1494726238, 1494726238, 2),
(57, 'country', 'CZ', 'admin', 1494726238, 1494726238, 2),
(58, 'country', 'DE', 'admin', 1494726238, 1494726238, 2),
(59, 'country', 'DJ', 'admin', 1494726238, 1494726238, 2),
(60, 'country', 'DK', 'admin', 1494726238, 1494726238, 2),
(61, 'country', 'DM', 'admin', 1494726238, 1494726238, 2),
(62, 'country', 'DO', 'admin', 1494726238, 1494726238, 2),
(63, 'country', 'DZ', 'admin', 1494726238, 1494726238, 2),
(64, 'country', 'EC', 'admin', 1494726238, 1494726238, 2),
(65, 'country', 'EE', 'admin', 1494726238, 1494726238, 2),
(66, 'country', 'EG', 'admin', 1494726238, 1494726238, 2),
(67, 'country', 'EH', 'admin', 1494726238, 1494726238, 2),
(68, 'country', 'ER', 'admin', 1494726238, 1494726238, 2),
(69, 'country', 'ES', 'admin', 1494726238, 1494726238, 2),
(70, 'country', 'ET', 'admin', 1494726238, 1494726238, 2),
(71, 'country', 'FI', 'admin', 1494726238, 1494726238, 2),
(72, 'country', 'FJ', 'admin', 1494726238, 1494726238, 2),
(73, 'country', 'FK', 'admin', 1494726238, 1494726238, 2),
(74, 'country', 'FM', 'admin', 1494726238, 1494726238, 2),
(75, 'country', 'FO', 'admin', 1494726238, 1494726238, 2),
(76, 'country', 'FR', 'admin', 1494726238, 1494726238, 2),
(77, 'country', 'GA', 'admin', 1494726238, 1494726238, 2),
(78, 'country', 'GB', 'admin', 1494726238, 1494726238, 2),
(79, 'country', 'GD', 'admin', 1494726238, 1494726238, 2),
(80, 'country', 'GE', 'admin', 1494726238, 1494726238, 2),
(81, 'country', 'GF', 'admin', 1494726238, 1494726238, 2),
(82, 'country', 'GG', 'admin', 1494726238, 1494726238, 2),
(83, 'country', 'GH', 'admin', 1494726238, 1494726238, 2),
(84, 'country', 'GI', 'admin', 1494726238, 1494726238, 2),
(85, 'country', 'GL', 'admin', 1494726238, 1494726238, 2),
(86, 'country', 'GM', 'admin', 1494726238, 1494726238, 2),
(87, 'country', 'GN', 'admin', 1494726238, 1494726238, 2),
(88, 'country', 'GP', 'admin', 1494726238, 1494726238, 2),
(89, 'country', 'GQ', 'admin', 1494726238, 1494726238, 2),
(90, 'country', 'GR', 'admin', 1494726238, 1494726238, 2),
(91, 'country', 'GS', 'admin', 1494726238, 1494726238, 2),
(92, 'country', 'GT', 'admin', 1494726238, 1494726238, 2),
(93, 'country', 'GU', 'admin', 1494726238, 1494726238, 2),
(94, 'country', 'GW', 'admin', 1494726238, 1494726238, 2),
(95, 'country', 'GY', 'admin', 1494726238, 1494726238, 2),
(96, 'country', 'HK', 'admin', 1494726238, 1494726238, 2),
(97, 'country', 'HM', 'admin', 1494726238, 1494726238, 2),
(98, 'country', 'HN', 'admin', 1494726238, 1494726238, 2),
(99, 'country', 'HR', 'admin', 1494726238, 1494726238, 2),
(100, 'country', 'HT', 'admin', 1494726238, 1494726238, 2),
(101, 'country', 'HU', 'admin', 1494726238, 1494726238, 2),
(102, 'country', 'ID', 'admin', 1494726238, 1494726238, 2),
(103, 'country', 'IE', 'admin', 1494726238, 1494726238, 2),
(104, 'country', 'IL', 'admin', 1494726238, 1494726238, 2),
(105, 'country', 'IM', 'admin', 1494726238, 1494726238, 2),
(106, 'country', 'IN', 'admin', 1494726238, 1494726238, 2),
(107, 'country', 'IO', 'admin', 1494726238, 1494726238, 2),
(108, 'country', 'IQ', 'admin', 1494726238, 1494726238, 2),
(109, 'country', 'IR', 'admin', 1494726238, 1494726238, 2),
(110, 'country', 'IS', 'admin', 1494726238, 1494726238, 2),
(111, 'country', 'IT', 'admin', 1494726238, 1494726238, 2),
(112, 'country', 'JE', 'admin', 1494726238, 1494726238, 2),
(113, 'country', 'JM', 'admin', 1494726238, 1494726238, 2),
(114, 'country', 'JO', 'admin', 1494726238, 1494726238, 2),
(115, 'country', 'JP', 'admin', 1494726238, 1494726238, 2),
(116, 'country', 'KE', 'admin', 1494726238, 1494726238, 2),
(117, 'country', 'KG', 'admin', 1494726238, 1494726238, 2),
(118, 'country', 'KH', 'admin', 1494726238, 1494726238, 2),
(119, 'country', 'KI', 'admin', 1494726238, 1494726238, 2),
(120, 'country', 'KM', 'admin', 1494726238, 1494726238, 2),
(121, 'country', 'KN', 'admin', 1494726238, 1494726238, 2),
(122, 'country', 'KP', 'admin', 1494726238, 1494726238, 2),
(123, 'country', 'KR', 'admin', 1494726238, 1494726238, 2),
(124, 'country', 'KW', 'admin', 1494726238, 1494726238, 2),
(125, 'country', 'KY', 'admin', 1494726238, 1494726238, 2),
(126, 'country', 'LA', 'admin', 1494726238, 1494726238, 2),
(127, 'country', 'LB', 'admin', 1494726238, 1494726238, 2),
(128, 'country', 'LC', 'admin', 1494726238, 1494726238, 2),
(129, 'country', 'LI', 'admin', 1494726238, 1494726238, 2),
(130, 'country', 'LK', 'admin', 1494726238, 1494726238, 2),
(131, 'country', 'LR', 'admin', 1494726238, 1494726238, 2),
(132, 'country', 'LS', 'admin', 1494726238, 1494726238, 2),
(133, 'country', 'LT', 'admin', 1494726238, 1494726238, 2),
(134, 'country', 'LU', 'admin', 1494726238, 1494726238, 2),
(135, 'country', 'LV', 'admin', 1494726238, 1494726238, 2),
(136, 'country', 'LY', 'admin', 1494726238, 1494726238, 2),
(137, 'country', 'MA', 'admin', 1494726238, 1494726238, 2),
(138, 'country', 'MC', 'admin', 1494726238, 1494726238, 2),
(139, 'country', 'MD', 'admin', 1494726238, 1494726238, 2),
(140, 'country', 'ME', 'admin', 1494726238, 1494726238, 2),
(141, 'country', 'MF', 'admin', 1494726238, 1494726238, 2),
(142, 'country', 'MG', 'admin', 1494726238, 1494726238, 2),
(143, 'country', 'MH', 'admin', 1494726238, 1494726238, 2),
(144, 'country', 'MK', 'admin', 1494726238, 1494726238, 2),
(145, 'country', 'ML', 'admin', 1494726238, 1494726238, 2),
(146, 'country', 'MM', 'admin', 1494726238, 1494726238, 2),
(147, 'country', 'MN', 'admin', 1494726238, 1494726238, 2),
(148, 'country', 'MO', 'admin', 1494726238, 1494726238, 2),
(149, 'country', 'MP', 'admin', 1494726238, 1494726238, 2),
(150, 'country', 'MQ', 'admin', 1494726238, 1494726238, 2),
(151, 'country', 'MR', 'admin', 1494726238, 1494726238, 2),
(152, 'country', 'MS', 'admin', 1494726238, 1494726238, 2),
(153, 'country', 'MT', 'admin', 1494726238, 1494726238, 2),
(154, 'country', 'MU', 'admin', 1494726238, 1494726238, 2),
(155, 'country', 'MV', 'admin', 1494726238, 1494726238, 2),
(156, 'country', 'MW', 'admin', 1494726238, 1494726238, 2),
(157, 'country', 'MX', 'admin', 1494726238, 1494726238, 2),
(158, 'country', 'MY', 'admin', 1494726238, 1494726238, 2),
(159, 'country', 'MZ', 'admin', 1494726238, 1494726238, 2),
(160, 'country', 'NA', 'admin', 1494726238, 1494726238, 2),
(161, 'country', 'NC', 'admin', 1494726238, 1494726238, 2),
(162, 'country', 'NE', 'admin', 1494726238, 1494726238, 2),
(163, 'country', 'NF', 'admin', 1494726238, 1494726238, 2),
(164, 'country', 'NG', 'admin', 1494726238, 1494726238, 2),
(165, 'country', 'NI', 'admin', 1494726238, 1494726238, 2),
(166, 'country', 'NL', 'admin', 1494726238, 1494726238, 2),
(167, 'country', 'NO', 'admin', 1494726238, 1494726238, 2),
(168, 'country', 'NP', 'admin', 1494726238, 1494726238, 2),
(169, 'country', 'NR', 'admin', 1494726238, 1494726238, 2),
(170, 'country', 'NU', 'admin', 1494726238, 1494726238, 2),
(171, 'country', 'NZ', 'admin', 1494726238, 1494726238, 2),
(172, 'country', 'OM', 'admin', 1494726238, 1494726238, 2),
(173, 'country', 'PA', 'admin', 1494726238, 1494726238, 2),
(174, 'country', 'PE', 'admin', 1494726238, 1494726238, 2),
(175, 'country', 'PF', 'admin', 1494726238, 1494726238, 2),
(176, 'country', 'PG', 'admin', 1494726238, 1494726238, 2),
(177, 'country', 'PH', 'admin', 1494726238, 1494726238, 2),
(178, 'country', 'PK', 'admin', 1494726238, 1494726238, 2),
(179, 'country', 'PL', 'admin', 1494726238, 1494726238, 2),
(180, 'country', 'PM', 'admin', 1494726238, 1494726238, 2),
(181, 'country', 'PN', 'admin', 1494726238, 1494726238, 2),
(182, 'country', 'PR', 'admin', 1494726238, 1494726238, 2),
(183, 'country', 'PS', 'admin', 1494726238, 1494726238, 2),
(184, 'country', 'PT', 'admin', 1494726238, 1494726238, 2),
(185, 'country', 'PW', 'admin', 1494726238, 1494726238, 2),
(186, 'country', 'PY', 'admin', 1494726238, 1494726238, 2),
(187, 'country', 'QA', 'admin', 1494726238, 1494726238, 2),
(188, 'country', 'RE', 'admin', 1494726238, 1494726238, 2),
(189, 'country', 'RO', 'admin', 1494726238, 1494726238, 2),
(190, 'country', 'RS', 'admin', 1494726238, 1494726238, 2),
(191, 'country', 'RU', 'admin', 1494726238, 1494726238, 2),
(192, 'country', 'RW', 'admin', 1494726238, 1494726238, 2),
(193, 'country', 'SA', 'admin', 1494726238, 1494726238, 2),
(194, 'country', 'SB', 'admin', 1494726238, 1494726238, 2),
(195, 'country', 'SC', 'admin', 1494726238, 1494726238, 2),
(196, 'country', 'SD', 'admin', 1494726238, 1494726238, 2),
(197, 'country', 'SE', 'admin', 1494726238, 1494726238, 2),
(198, 'country', 'SG', 'admin', 1494726238, 1494726238, 2),
(199, 'country', 'SH', 'admin', 1494726238, 1494726238, 2),
(200, 'country', 'SI', 'admin', 1494726238, 1494726238, 2),
(201, 'country', 'SJ', 'admin', 1494726238, 1494726238, 2),
(202, 'country', 'SK', 'admin', 1494726238, 1494726238, 2),
(203, 'country', 'SL', 'admin', 1494726238, 1494726238, 2),
(204, 'country', 'SM', 'admin', 1494726238, 1494726238, 2),
(205, 'country', 'SN', 'admin', 1494726238, 1494726238, 2),
(206, 'country', 'SO', 'admin', 1494726238, 1494726238, 2),
(207, 'country', 'SR', 'admin', 1494726238, 1494726238, 2),
(208, 'country', 'SS', 'admin', 1494726238, 1494726238, 2),
(209, 'country', 'ST', 'admin', 1494726238, 1494726238, 2),
(210, 'country', 'SV', 'admin', 1494726238, 1494726238, 2),
(211, 'country', 'SY', 'admin', 1494726238, 1494726238, 2),
(212, 'country', 'SZ', 'admin', 1494726238, 1494726238, 2),
(213, 'country', 'TC', 'admin', 1494726238, 1494726238, 2),
(214, 'country', 'TD', 'admin', 1494726238, 1494726238, 2),
(215, 'country', 'TF', 'admin', 1494726238, 1494726238, 2),
(216, 'country', 'TG', 'admin', 1494726238, 1494726238, 2),
(217, 'country', 'TH', 'admin', 1494726238, 1494726238, 2),
(218, 'country', 'TJ', 'admin', 1494726238, 1494726238, 2),
(219, 'country', 'TK', 'admin', 1494726238, 1494726238, 2),
(220, 'country', 'TL', 'admin', 1494726238, 1494726238, 2),
(221, 'country', 'TM', 'admin', 1494726238, 1494726238, 2),
(222, 'country', 'TN', 'admin', 1494726238, 1494726238, 2),
(223, 'country', 'TO', 'admin', 1494726238, 1494726238, 2),
(224, 'country', 'TR', 'admin', 1494726238, 1494726238, 2),
(225, 'country', 'TT', 'admin', 1494726238, 1494726238, 2),
(226, 'country', 'TV', 'admin', 1494726238, 1494726238, 2),
(227, 'country', 'TW', 'admin', 1494726238, 1494726238, 2),
(228, 'country', 'TZ', 'admin', 1494726238, 1494726238, 2),
(229, 'country', 'UA', 'admin', 1494726238, 1494726238, 2),
(230, 'country', 'UG', 'admin', 1494726238, 1494726238, 2),
(231, 'country', 'UM', 'admin', 1494726238, 1494726238, 2),
(232, 'country', 'US', 'admin', 1494726238, 1494726238, 2),
(233, 'country', 'UY', 'admin', 1494726238, 1494726238, 2),
(234, 'country', 'UZ', 'admin', 1494726238, 1494726238, 2),
(235, 'country', 'VA', 'admin', 1494726238, 1494726238, 2),
(236, 'country', 'VC', 'admin', 1494726238, 1494726238, 2),
(237, 'country', 'VE', 'admin', 1494726238, 1494726238, 2),
(238, 'country', 'VG', 'admin', 1494726238, 1494726238, 2),
(239, 'country', 'VI', 'admin', 1494726238, 1494726238, 2),
(240, 'country', 'VN', 'admin', 1494726238, 1494726238, 2),
(241, 'country', 'VU', 'admin', 1494726238, 1494726238, 2),
(242, 'country', 'WF', 'admin', 1494726238, 1494726238, 2),
(243, 'country', 'WS', 'admin', 1494726238, 1494726238, 2),
(244, 'country', 'YE', 'admin', 1494726238, 1494726238, 2),
(245, 'country', 'YT', 'admin', 1494726238, 1494726238, 2),
(246, 'country', 'ZA', 'admin', 1494726238, 1494726238, 2),
(247, 'country', 'ZM', 'admin', 1494726238, 1494726238, 2),
(248, 'country', 'ZW', 'admin', 1494726238, 1494726238, 2),
(249, 'song', '1', 'admin', 1494906062, 1494906062, 2),
(250, 'song', '2', 'admin', 1494906142, 1494906142, 2),
(251, 'song', '3', 'admin', 1494906778, 1494906778, 2),
(252, 'song', '4', 'admin', 1494906843, 1494906843, 2),
(253, 'song', '5', 'admin', 1494906986, 1494906986, 2),
(254, 'radiosong', '1', 'admin', 1494944512, 1494944512, 2);

-- --------------------------------------------------------

--
-- Table structure for table `membership_users`
--

CREATE TABLE `membership_users` (
  `memberID` varchar(20) NOT NULL,
  `passMD5` varchar(40) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `signupDate` date DEFAULT NULL,
  `groupID` int(10) UNSIGNED DEFAULT NULL,
  `isBanned` tinyint(4) DEFAULT NULL,
  `isApproved` tinyint(4) DEFAULT NULL,
  `custom1` text,
  `custom2` text,
  `custom3` text,
  `custom4` text,
  `comments` text,
  `pass_reset_key` varchar(100) DEFAULT NULL,
  `pass_reset_expiry` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `membership_users`
--

INSERT INTO `membership_users` (`memberID`, `passMD5`, `email`, `signupDate`, `groupID`, `isBanned`, `isApproved`, `custom1`, `custom2`, `custom3`, `custom4`, `comments`, `pass_reset_key`, `pass_reset_expiry`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3', 'info@lddevelop.com', '2017-05-13', 2, 0, 1, 'Bart Vertongen', 'Geraardsbergenstraat 35', 'Ronse', 'Oost Vlaanderen', 'Admin member created automatically on 2017-05-13\nRecord updated automatically on 2017-05-14', NULL, NULL),
('guest', NULL, NULL, '2017-05-13', 1, 0, 1, NULL, NULL, NULL, NULL, 'Anonymous member created automatically on 2017-05-13', NULL, NULL),
('ivars', '3f7ca0a8d1ada3b390532918556681d4', 'info@lddevelop.com', '2017-05-14', 2, 0, 1, 'Ivars Kasickis', '', '', 'Latvia', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `radiosong`
--

CREATE TABLE `radiosong` (
  `ROS_ID` bigint(20) UNSIGNED NOT NULL,
  `ROS_SNG_ID` bigint(20) UNSIGNED NOT NULL,
  `ROS_RDO_ID` bigint(20) UNSIGNED NOT NULL,
  `ROS_START` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ROS_END` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `radiosong`
--

INSERT INTO `radiosong` (`ROS_ID`, `ROS_SNG_ID`, `ROS_RDO_ID`, `ROS_START`, `ROS_END`) VALUES
(1, 1, 1, '2017-05-15 14:21:00', '2017-05-15 14:23:00');

-- --------------------------------------------------------

--
-- Table structure for table `radiostation`
--

CREATE TABLE `radiostation` (
  `RDO_ID` bigint(20) UNSIGNED NOT NULL,
  `RDO_ShortName` varchar(30) NOT NULL,
  `RDO_LongName` varchar(80) NOT NULL,
  `RDO_Website` varchar(80) NOT NULL,
  `RDO_Logo` varchar(40) DEFAULT NULL,
  `RDO_CTR_Code` char(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `radiostation`
--

INSERT INTO `radiostation` (`RDO_ID`, `RDO_ShortName`, `RDO_LongName`, `RDO_Website`, `RDO_Logo`, `RDO_CTR_Code`) VALUES
(1, 'EHR', 'European Hit Radio', 'EHR.com', '17157600_1494690862.png', 'LV'),
(2, 'SLAM', 'Slam Fm', 'Slamfm.com', NULL, 'LV');

-- --------------------------------------------------------

--
-- Table structure for table `song`
--

CREATE TABLE `song` (
  `SNG_ID` bigint(20) UNSIGNED NOT NULL,
  `SNG_Title` varchar(80) NOT NULL,
  `SNG_Artist` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `song`
--

INSERT INTO `song` (`SNG_ID`, `SNG_Title`, `SNG_Artist`) VALUES
(1, 'No Promises', 'Cheat Codes & Demi Lovato'),
(2, 'Something Just Like his', 'Chainsmokers & Coldplay'),
(3, 'It Ain\'t Me', 'Kygo & Selena Gomez'),
(4, 'Now Or Never', 'Halsey'),
(5, 'Galway Girl', 'Ed Sheerman');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`CTR_Code`),
  ADD UNIQUE KEY `CTR_Name_unique` (`CTR_Name`);

--
-- Indexes for table `membership_grouppermissions`
--
ALTER TABLE `membership_grouppermissions`
  ADD PRIMARY KEY (`permissionID`);

--
-- Indexes for table `membership_groups`
--
ALTER TABLE `membership_groups`
  ADD PRIMARY KEY (`groupID`);

--
-- Indexes for table `membership_userpermissions`
--
ALTER TABLE `membership_userpermissions`
  ADD PRIMARY KEY (`permissionID`);

--
-- Indexes for table `membership_userrecords`
--
ALTER TABLE `membership_userrecords`
  ADD PRIMARY KEY (`recID`),
  ADD UNIQUE KEY `tableName_pkValue` (`tableName`,`pkValue`),
  ADD KEY `pkValue` (`pkValue`),
  ADD KEY `tableName` (`tableName`),
  ADD KEY `memberID` (`memberID`),
  ADD KEY `groupID` (`groupID`);

--
-- Indexes for table `membership_users`
--
ALTER TABLE `membership_users`
  ADD PRIMARY KEY (`memberID`),
  ADD KEY `groupID` (`groupID`);

--
-- Indexes for table `radiosong`
--
ALTER TABLE `radiosong`
  ADD PRIMARY KEY (`ROS_ID`),
  ADD UNIQUE KEY `UNIQ_SONG_RADIO` (`ROS_SNG_ID`,`ROS_RDO_ID`),
  ADD KEY `ROS_SNG_ID` (`ROS_SNG_ID`),
  ADD KEY `ROS_RDO_ID` (`ROS_RDO_ID`);

--
-- Indexes for table `radiostation`
--
ALTER TABLE `radiostation`
  ADD PRIMARY KEY (`RDO_ID`),
  ADD UNIQUE KEY `RDO_ShortName_unique` (`RDO_ShortName`),
  ADD KEY `RDO_CTR_Code` (`RDO_CTR_Code`);

--
-- Indexes for table `song`
--
ALTER TABLE `song`
  ADD PRIMARY KEY (`SNG_ID`),
  ADD UNIQUE KEY `SNG_Title_unique` (`SNG_Title`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `membership_grouppermissions`
--
ALTER TABLE `membership_grouppermissions`
  MODIFY `permissionID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `membership_groups`
--
ALTER TABLE `membership_groups`
  MODIFY `groupID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `membership_userpermissions`
--
ALTER TABLE `membership_userpermissions`
  MODIFY `permissionID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `membership_userrecords`
--
ALTER TABLE `membership_userrecords`
  MODIFY `recID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=255;
--
-- AUTO_INCREMENT for table `radiosong`
--
ALTER TABLE `radiosong`
  MODIFY `ROS_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `radiostation`
--
ALTER TABLE `radiostation`
  MODIFY `RDO_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `song`
--
ALTER TABLE `song`
  MODIFY `SNG_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
